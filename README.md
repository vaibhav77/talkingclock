# Talking Clock



## Description
Within the team, we don't like clocks that display as numbers. In fact, we like clocks that present the current time in a more "Human Friendly" way.
This Spring boot project exposes a rest endpoint to return the time in a more "Human Friendly" way demonstrated in the example below.

Numeric Time Human Friendly Text
<li>1:00 -> One o'clock</li>
<li>2:00 -> Two o'clock</li>
<li>13:00 -> One o'clock</li>
<li>13:05 -> Five past one</li>
<li>13:10 -> Ten past one</li>
<li>13:25 -> Twenty five past one</li>
<li>13:30 -> Half past one</li>
<li>13:35 -> Twenty five to two</li>
<li>13:55 -> Five to two</li>

## Installation
To run this project we need Java 11 and Maven installed

## Swagger
http://localhost:5000/clock/api/v1/swagger-ui/index.html