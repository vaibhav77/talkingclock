package com.vaibhav.programing.talkingclock;

import com.vaibhav.programing.talkingclock.controller.TalkingClockController;
import com.vaibhav.programing.talkingclock.service.TalkingClockService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(TalkingClockController.class)
public class TalkingClockControllerTest {

    @MockBean
    TalkingClockService talkingClockService;

    @Autowired
    MockMvc mockMvc;


    @Test
    public void testGivenTime() throws Exception {
        Mockito.when(talkingClockService.getTalkingTime("12:30"))
                .thenReturn("half past Twelve");

        MvcResult result = mockMvc.perform(get("/talking-clock/current-time?time=12:30"))
                .andExpect(status().isOk()).andReturn();
        String resultDOW = result.getResponse().getContentAsString();
        assertNotNull(resultDOW);
        assertEquals("half past Twelve", resultDOW);
    }

    @Test
    public void testCurrentTime() throws Exception {
        Mockito.when(talkingClockService.getTalkingTime(null))
                .thenReturn("half past Twelve");

        MvcResult result = mockMvc.perform(get("/talking-clock/current-time"))
                .andExpect(status().isOk())
                .andReturn();
        String resultDOW = result.getResponse().getContentAsString();
        assertNotNull(resultDOW);
        assertEquals("half past Twelve", resultDOW);
    }

    @Test
    public void testIllegalFormatTime() throws Exception {
        String errorMsg = "invalid time, format expected hh:mm";
        Mockito.when(talkingClockService.getTalkingTime("23,30"))
                .thenThrow(new IllegalArgumentException(errorMsg));

        MvcResult result = mockMvc.perform(get("/talking-clock/current-time?time=23,30"))
                .andExpect(status().isBadRequest())
                .andReturn();
        String resultDOW = result.getResponse().getContentAsString();
        assertNotNull(resultDOW);
        assertEquals(errorMsg, resultDOW);
    }

}
