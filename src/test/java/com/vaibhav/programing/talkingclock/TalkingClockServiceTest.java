package com.vaibhav.programing.talkingclock;

import com.vaibhav.programing.talkingclock.service.TalkingClockService;
import com.vaibhav.programing.talkingclock.util.TimeUtil;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalTime;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@SpringBootTest
public class TalkingClockServiceTest {

    @InjectMocks
    private TalkingClockService talkingClockService = new TalkingClockService();

    @Mock
    TimeUtil timeUtil;

    @Test
    public void testCurrentTime(){
        String expectedTime = "half past Twelve";
        LocalTime localTime = LocalTime.of(12,30);
        when(timeUtil.getCurrentTime()).thenReturn(localTime);

        String actualTime = talkingClockService.getTalkingTime(null);
        assertEquals(expectedTime,actualTime);
    }

    @Test
    public void testGivenTime(){
        String expectedTime = "half past Twelve";
        String inputTime = "12:30";
        String actualTime = talkingClockService.getTalkingTime(inputTime);
        assertEquals(expectedTime,actualTime);
    }

    @ParameterizedTest
    @ValueSource(strings = {"  ","27:35","12:67","-12:30","12,30"})
    public void testInvalidTime(String input){
        String expectedError = "invalid time, format expected hh:mm";
        Exception thrown = assertThrows(IllegalArgumentException.class,
                ()->talkingClockService.getTalkingTime(input));
        assertTrue(thrown.getMessage().contains(expectedError));
    }

    @ParameterizedTest
    @CsvSource(value = {"12:15|quarter past Twelve","12:30|half past Twelve", "12:45|quarter to Thirteen",
            "12:29|Twenty nine minutes past Twelve", "12:35|Twenty five minutes to Thirteen"}
            , delimiter = '|')
    void testGivenTimeSpecialCases(String input, String expected) {
        String actualValue = talkingClockService.getTalkingTime(input);
        assertEquals(expected, actualValue);
    }
}
