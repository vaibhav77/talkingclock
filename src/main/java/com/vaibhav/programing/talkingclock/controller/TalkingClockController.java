package com.vaibhav.programing.talkingclock.controller;

import com.vaibhav.programing.talkingclock.service.TalkingClockService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

/**
 * This controller returns the current time in UTC or the given time (HH:MM) to a more human-readable format
 *
 * @author Vaibhav Srivastava
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/talking-clock")
public class TalkingClockController {

    @Autowired
    private TalkingClockService talkingClockService;

    /**
     * <p>This endpoint returns the current time in UTC or the given time (HH:MM) to a more human-readable format
     * </p>
     *
     * @param time optional input time in HH:MM format
     * @return time in human-readable format
     * @since 1.0
     */
    @GetMapping("/current-time")
    public ResponseEntity<String> fetchCurrentTime(@RequestParam(required = false) String time) {
        return Optional.ofNullable(talkingClockService.getTalkingTime(time))
                .map(response -> new ResponseEntity<>(response, HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.BAD_REQUEST));
    }
    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<String> handleProductException(IllegalArgumentException businessRuleServiceException){
        return new ResponseEntity<>(businessRuleServiceException.getMessage(),HttpStatus.BAD_REQUEST);
    }


}
