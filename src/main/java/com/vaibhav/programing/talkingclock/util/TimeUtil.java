package com.vaibhav.programing.talkingclock.util;

import org.springframework.stereotype.Service;

import java.time.Instant;
import java.time.LocalTime;
import java.time.ZoneOffset;

/**
 * This the Class handles time in UTC
 *
 * @author Vaibhav Srivastava
 */
@Service
public class TimeUtil {

    /**
     * <p>This method returns the current time in UTC
     * </p>
     *
     * @return current time in UTC
     * @since 1.0
     */
    public LocalTime getCurrentTime(){
        Instant instant = Instant.now();
        return LocalTime.ofInstant(instant, ZoneOffset.UTC);
    }
}
