package com.vaibhav.programing.talkingclock.service;

import com.vaibhav.programing.talkingclock.util.TimeUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalTime;

/**
 * This service returns the current time in UTC or the given time (HH:MM) to a more human-readable format
 *
 * @author Vaibhav Srivastava
 */
@Service
@RequiredArgsConstructor
public class TalkingClockService {

    @Autowired
    private TimeUtil timeUtil;
    private final String[] timeMapping = {"Zero", "One", "Two", "Three", "Four",
            "Five", "Six", "Seven", "Eight", "Nine",
            "Ten", "Eleven", "Twelve", "Thirteen",
            "Fourteen", "Fifteen", "Sixteen", "Seventeen",
            "Eighteen", "Nineteen", "Twenty", "Twenty one",
            "Twenty two", "Twenty three", "Twenty four",
            "Twenty five", "Twenty six", "Twenty seven",
            "Twenty eight", "Twenty nine",
    };

    /**
     * <p>This method returns the current time in UTC or the given time (HH:MM) to a more human-readable format
     * </p>
     *
     * @param inputTime input time in HH:MM format
     * @return time in human-readable format
     * @since 1.0
     */
    public String getTalkingTime(String inputTime) {
        if (inputTime == null || inputTime.isEmpty()) {
            LocalTime currentTime = timeUtil.getCurrentTime();
            return convertTalkingTime(currentTime.getHour(), currentTime.getMinute());
        } else {
            String[] time = inputTime.split(":");
            if (time.length != 2 || Integer.parseInt(time[0]) > 24 || Integer.parseInt(time[1]) > 60
                    || Integer.parseInt(time[0]) < 0 || Integer.parseInt(time[1]) < 0) {
                throw new IllegalArgumentException("invalid time, format expected hh:mm");
            }
            return convertTalkingTime(Integer.parseInt(time[0]), Integer.parseInt(time[1]));
        }
    }

    /**
     * <p>This method returns the given time (HH:MM) to a more human-readable format
     * </p>
     *
     * @param h input hour
     * @param m input miutes
     * @return time in human-readable format
     * @since 1.0
     */
    private String convertTalkingTime(int h, int m) {
        StringBuilder talkingTime = new StringBuilder();

        if (m == 0)
            talkingTime.append(timeMapping[h]).append(" o' clock ");

        else if (m == 15)
            talkingTime.append("quarter past ").append(timeMapping[h]);

        else if (m == 30)
            talkingTime.append("half past ").append(timeMapping[h]);

        else if (m == 45)
            talkingTime.append("quarter to ").append(timeMapping[h + 1]);

        else if (m <= 30)
            talkingTime.append(timeMapping[m]).append(" minutes past ").append(timeMapping[h]);

        else talkingTime.append(timeMapping[60 - m]).append(" minutes to ").append(timeMapping[h + 1]);

        return talkingTime.toString();
    }


}
